using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;

namespace TestAutomationChannel9
{
    class AutomationTest
    {
        private IWebDriver driver;
        public string homeURL;
        public string loginXPath = "//*[@id='content']/div/div[2]/div/div[1]/header/div/a[2]";
        public string loginPageUrl = "https://login.nine.com.au/login?client_id=9nowweb&redirect_uri=%2F&category=9now_web&site=9now_Home&section=default&cta=9n_home_sitenav_sso_login";
        public string loggedInUserXPath = "//*[@id='content']/div/div[2]/div/div[1]/header/div/div/div/ul/li[1]/a";
        public string userName = "troyincarnate@gmail.com";
        public string password = "omitted";

        [Test(Description = "Check 9now for Login Link")]
        public void Login_is_on_home_page()
        {
            // Test login button
            // navigate to the website
            driver.Navigate().GoToUrl(homeURL);

            // wait 15 seconds to locate sign in element
            WebDriverWait wait = Wait();

            // use the driver to search href = login and once it has a value or timeout, complete testing
            wait.Until(driver =>
            driver.FindElement(By.XPath(loginXPath))); // (By.XPath("//a[@href='/beta/login']")));
            //    // "//*[@id='content']/div/div[2]/div/div[1]/header/div/a[2]"
            // use same method to get value
            IWebElement element =
            driver.FindElement(By.XPath(loginXPath));
            // now check if expected hardcoded value is the same as value retrieved
            Assert.AreEqual("Log in", element.GetAttribute("text"));
        }

        [Test(Description = "Check Navigation to login page")]
        public void Test_nav_to_login_page()
        {
            driver.Navigate().GoToUrl(homeURL);
            WebDriverWait wait = Wait();
            wait.Until(driver =>
            driver.FindElement(By.LinkText("Log in")));

            // Test nav into the login page
            driver.FindElement(By.LinkText("Log in")).Click();
            Assert.AreEqual(loginPageUrl, driver.Url);
        }

        [Test(Description = "Logging in to website and test logged in")]
        public void Test_nav_and_login()
        {
            driver.Navigate().GoToUrl(homeURL);
            WebDriverWait wait = Wait();
            wait.Until(driver =>
            driver.FindElement(By.LinkText("Log in")));
            driver.FindElement(By.LinkText("Log in")).Click();

            // now look for login box and fill it in
            wait.Until(driver =>
            driver.FindElement(By.Name("email")));
            IWebElement loginBox = driver.FindElement(By.Name("email"));
            loginBox.SendKeys(userName);
            driver.FindElement(By.XPath("//*[@id='app']/div/div[1]/div[3]/div/div/div[3]/div/form/div[2]/button")).Submit();

            // now look for password box and fill in password
            wait.Until(driver =>
            driver.FindElement(By.Name("password")));
            IWebElement passwordBox = driver.FindElement(By.Name("password"));
            passwordBox.SendKeys(password);
            driver.FindElement(By.XPath("//*[@id='app']/div/div[1]/div[3]/div/div/div[2]/div/form/div[2]/button")).Submit();

            // wait until the itemtype value is Troydon before exiting page
            wait.Until(driver =>
            driver.FindElement(By.XPath(loggedInUserXPath)));
            // driver.FindElement(By.XPath("//a[@href='/beta/login']"));

            // Check if the My Profile is visible to show user is logged in
            IWebElement personLoggedIn =
                driver.FindElement(By.XPath(loggedInUserXPath));
            Assert.AreEqual("My Profile", personLoggedIn.GetAttribute("text"));
        }

        [Test(Description = "Logging in and list the third item in watch history")]
        public void List_3rd_video_watch_history()
        {
            WebDriverWait wait = Login();

            // now verify watch history
            wait.Until(driver =>
                driver.FindElement(By.LinkText("Watch History")));
            driver.FindElement(By.LinkText("Watch History")).Click();

            wait.Until(driver =>
                driver.FindElement(By.XPath("//h4[contains(text(),'Emergence')]")));
            IWebElement emergenceName =
                driver.FindElement(By.XPath("//h4[contains(text(),'Emergence')]"));
              Assert.AreEqual(null, emergenceName.GetAttribute("text"));
        }

        [Test(Description = "Check if we can navigate to the emergence video")]
        public void Navigate_to_vid_in_watch_history_vid()
        {
            WebDriverWait wait = Login();
            string emergenceUrl = "https://www.9now.com.au/emergence/season-1";
            // now verify watch history
            wait.Until(driver =>
                driver.FindElement(By.LinkText("Watch History")));
            driver.FindElement(By.LinkText("Watch History")).Click();

            wait.Until(driver =>
               driver.FindElement(By.XPath("//a[@href='/emergence/season-1']")));
            driver.FindElement(By.XPath("//a[@href='/emergence/season-1']")).Click();
            Assert.AreEqual(emergenceUrl, driver.Url);
        }

        [Test(Description = "Test we can load a video URL")]
        public void Verify_ep1_nav_emergence()
        {
            WebDriverWait wait = Login();
            string emergencep1Url = "https://www.9now.com.au/emergence/season-1/episode-1";

            // now verify watch history
            wait.Until(driver =>
                driver.FindElement(By.LinkText("Watch History")));
            driver.FindElement(By.LinkText("Watch History")).Click();

            wait.Until(driver =>
               driver.FindElement(By.XPath("//a[@href='/emergence/season-1']")));
            driver.FindElement(By.XPath("//a[@href='/emergence/season-1']")).Click();

            wait.Until(driver =>
              driver.FindElement(By.XPath("//a[@href='/emergence/season-1/episode-1']")));
            driver.FindElement(By.XPath("//a[@href='/emergence/season-1/episode-1']")).Click();
            Assert.AreEqual(emergencep1Url, driver.Url);
        }

        [Test(Description = "Test we can play vid")]
        public void Verify_play()
        {
            WebDriverWait wait = Login();
            string emergencep1Url = "https://www.9now.com.au/emergence/season-1/episode-1";

            // now verify watch history
            wait.Until(driver =>
                driver.FindElement(By.LinkText("Watch History")));
            driver.FindElement(By.LinkText("Watch History")).Click();

            wait.Until(driver =>
               driver.FindElement(By.XPath("//a[@href='/emergence/season-1']")));
            driver.FindElement(By.XPath("//a[@href='/emergence/season-1']")).Click();

            wait.Until(driver =>
              driver.FindElement(By.XPath("//a[@href='/emergence/season-1/episode-1']")));
            driver.FindElement(By.XPath("//a[@href='/emergence/season-1/episode-1']")).Click();

            // check to see value is set to paused
            wait.Until(driver =>
                driver.FindElement(By.XPath("//*[@id='player_xwy8kn']/div[11]/button[1]")));
          //  driver.FindElement(By.XPath("//*[@id='player_bbpzag']/div[7]/button[1]")).Click(); 
            IWebElement vidStatus =
                driver.FindElement(By.XPath("//button[@class='vjs-play-control']"));
            Assert.AreEqual("Pause", vidStatus.GetAttribute("text")); // if value is paused means its currently playing (neeed to wait 20s for auto resume)
        }

        [Test(Description = "Test we can pause video")]
        public void Verify_pause()
        {
            WebDriverWait wait = Login();
            string emergencep1Url = "https://www.9now.com.au/emergence/season-1/episode-1";

            // now verify watch history
            wait.Until(driver =>
                driver.FindElement(By.LinkText("Watch History")));
            driver.FindElement(By.LinkText("Watch History")).Click();

            wait.Until(driver =>
               driver.FindElement(By.XPath("//a[@href='/emergence/season-1']")));
            driver.FindElement(By.XPath("//a[@href='/emergence/season-1']")).Click();

            wait.Until(driver =>
              driver.FindElement(By.XPath("//a[@href='/emergence/season-1/episode-1']")));
            driver.FindElement(By.XPath("//a[@href='/emergence/season-1/episode-1']")).Click();

            // play video
            wait.Until(driver =>
              driver.FindElement(By.XPath("//button[@class='vjs-play-control']")));
            driver.FindElement(By.XPath("//button[@class='vjs-play-control']")).Click();

            // check to see value is set to paused
            wait.Until(driver =>
    driver.FindElement(By.XPath("//*[@id='player_bbpzag']/div[7]/button[1]")));
            driver.FindElement(By.XPath("//*[@id='player_bbpzag']/div[7]/button[1]")).Click();
            IWebElement vidStatus =
                driver.FindElement(By.XPath("//*[@id='player_bbpzag']/div[7]/button[1]"));
            Assert.AreEqual("Play", vidStatus.GetAttribute("text"));
        }

        private WebDriverWait Login()
        {
            driver.Navigate().GoToUrl(homeURL);
            WebDriverWait wait = Wait();
            wait.Until(driver =>
                driver.FindElement(By.LinkText("Log in")));
            driver.FindElement(By.LinkText("Log in")).Click();

            wait.Until(driver =>
            driver.FindElement(By.Name("email")));
            IWebElement loginBox = driver.FindElement(By.Name("email"));
            loginBox.SendKeys(userName);
            driver.FindElement(By.XPath("//*[@id='app']/div/div[1]/div[3]/div/div/div[3]/div/form/div[2]/button")).Submit();

            wait.Until(driver =>
            driver.FindElement(By.Name("password")));
            IWebElement passwordBox = driver.FindElement(By.Name("password"));
            passwordBox.SendKeys(password);
            driver.FindElement(By.XPath("//*[@id='app']/div/div[1]/div[3]/div/div/div[2]/div/form/div[2]/button")).Submit();

            wait.Until(driver =>
            driver.FindElement(By.XPath(loggedInUserXPath)));
            IWebElement personLoggedIn =
                driver.FindElement(By.XPath(loggedInUserXPath));
            return wait;
        }

        private WebDriverWait Wait()
        {
            return new WebDriverWait(driver,
                        System.TimeSpan.FromSeconds(15));
        }

        [TearDown]
        public void TearDownTest()
        {
            driver.Close();
        }

        [SetUp]
        public void SetupTest()
        {
            homeURL = "https://www.9now.com.au/";
            driver = new ChromeDriver("D:\\3rdparty\\chrome");
        }
    }
}
